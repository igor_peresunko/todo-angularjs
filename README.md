* Задача: название, описание, время
* Список задач: название, дата, список задач
* Отметка о выполнении задачи
* CRUD
* Сохранение в базу. Желательно использовать Postgres (или SQLite).
* Интерфейс на Bootstrap или аналогичной штуковине

## Installation

1. Create a folder on the local drive and clone the repository ```https://igor_peresunko@bitbucket.org/igor_peresunko/todo-angularjs.git```
2. Navigate to the folder *../angular*
3. To create a virtual environment ```virtualenv env```
4. Run virtual environment ```. env/bin/activate```
5. Install all packages from file __requirements.txt__ ```pip install -r requirements.txt```
6. Navigate to the folder *../todo_project*
7. To run the command ```python manage.py migrate```
8. To create a superuser ```python manage.py createsuperuser```
9. Start the server and go to the link http://127.0.0.1:8000/