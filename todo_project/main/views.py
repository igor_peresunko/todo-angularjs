from django.shortcuts import render


def home(request):
    """Method to display the home page"""
    return render(request, 'main/home.html')
