from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.reverse import reverse
from rest_framework.decorators import api_view
from rest_framework.response import Response

from serializers import TodoListSerializer, TodoSerializer
from permissions import IsTodoListOwner, IsTodoOwner
from todo.models import TodoList, Todo


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'list': reverse('list-list', request=request, format=format),
        'todo': reverse('todo-list', request=request, format=format)
    })


class TodoListViewSet(viewsets.ModelViewSet):
    """ViewSet to view information about the lists"""
    serializer_class = TodoListSerializer
    permission_classes = (permissions.IsAuthenticated, IsTodoListOwner)

    def get_queryset(self):
        return TodoList.objects.filter(owner=self.request.user)


class TodoViewSet(viewsets.ModelViewSet):
    """ViewSet to view information about the todos"""
    serializer_class = TodoSerializer
    permission_classes = (permissions.IsAuthenticated, IsTodoOwner)

    def get_queryset(self):
        return Todo.objects.filter(todo_list__owner=self.request.user)
