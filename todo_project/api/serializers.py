from rest_framework import serializers

from todo.models import TodoList, Todo


class TodoListSerializer(serializers.ModelSerializer):
    """Serializer to store data about lists"""
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = TodoList
        fields = ('id', 'name_of_list', 'pub_date', 'owner', 'count_tasks')
        read_only_fields = ('id', 'name_of_list', 'pub_date', 'owner', 'count_tasks')


class TodoSerializer(serializers.ModelSerializer):
    """Serializer to store information about todo"""

    class Meta:
        model = Todo
        fields = ('id', 'pub_date', 'todo_list', 'name_of_todo', 'task', 'status')
        read_only_fields = ('id', 'pub_date')
