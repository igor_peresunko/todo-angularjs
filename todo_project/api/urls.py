from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

import views

router = DefaultRouter()
router.register(r'lists', views.TodoListViewSet, base_name='lists')
router.register(r'todo', views.TodoViewSet, base_name='todo')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
