from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.TodoListView.as_view(), name='todo_list'),
    url(r'^create_list$', views.TodoListCreateView.as_view(), name='create_list'),
    url(r'^(?P<pk>[0-9]+)/$', views.TodosView.as_view(), name='todos'),
    url(r'^(?P<pk>[0-9]+)/remove/$', views.RemoveTodoListView.as_view(), name='remove_list'),
    url(r'^(?P<pk>[0-9]+)/rename_list/$', views.RenameTodoListView.as_view(), name='rename_todo_list'),

    url(r'^(?P<list_pk>[0-9]+)/create_todo/$', views.CreateTodoView.as_view(), name='create_todo'),
    url(r'^(?P<todolist_pk>[0-9]+)/(?P<pk>[0-9]+)/remove_todo/$', views.RemoveTodoView.as_view(), name='remove_todo'),
    url(r'^(?P<todolist_pk>[0-9]+)/(?P<pk>[0-9]+)/edit/$', views.EditTodoView.as_view(), name='edit_todo'),
    url(r'^(?P<todolist_pk>[0-9]+)/(?P<pk>[0-9]+)/$', views.TodoDetailView.as_view(), name='todo_detail'),

]
