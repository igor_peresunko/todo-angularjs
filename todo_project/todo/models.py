from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings


class TodoList(models.Model):
    """Model for storing a list of names and dates of creation"""

    name_of_list = models.CharField(max_length=100)
    pub_date = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __unicode__(self):
        return self.name_of_list

    def count_tasks(self):
        return self.todo_set.count()

    def get_absolute_url(self):
        return reverse('todo:todos', kwargs={'pk': self.id})


class Todo(models.Model):
    """Model for the storage of information on the todo"""

    todo_list = models.ForeignKey(TodoList)
    name_of_todo = models.CharField(max_length=101)
    pub_date = models.DateTimeField(auto_now_add=True)
    task = models.TextField(help_text='Description of the problem')
    status = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name_of_todo

    def get_absolute_url(self):
        return reverse('todo:todo_detail', kwargs={'todolist_pk': self.todo_list.id, 'pk': self.id})
