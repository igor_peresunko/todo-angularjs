from django.contrib import admin

from .models import TodoList, Todo

admin.site.register(TodoList)
admin.site.register(Todo)


class TodoListAdmin(admin.ModelAdmin):
    list_display = ('name_of_list', 'pub_date')


class TaskInLine(admin.StackedInline):
    model = Todo
    extra = 3
