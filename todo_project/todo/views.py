from django.views.generic import ListView, DetailView, DeleteView, UpdateView
from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404
from .models import Todo, TodoList


class TodoListView(ListView):  # correct
    """Displays all the lists"""

    model = TodoList
    template_name = 'todo/todo_lists.html'
    context_object_name = 'todolist'

    def get_queryset(self):
        user = self.request.user
        if self.request.user.is_authenticated():
            lists = TodoList.objects.filter(owner=user)
            return lists


class TodoListCreateView(CreateView):  # correct
    """Create a list"""

    model = TodoList
    template_name = 'todo/create_list.html'
    fields = ['name_of_list']

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(TodoListCreateView, self).form_valid(form)


class RenameTodoListView(UpdateView):  # correct
    """Renames the current list"""

    model = TodoList
    template_name = 'todo/rename_list.html'
    fields = ['name_of_list', ]

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(RenameTodoListView, self).form_valid(form)


class RemoveTodoListView(DeleteView):  # correct
    """Deletes the current list"""

    model = TodoList
    template_name = 'todo/remove_list.html'
    success_url = reverse_lazy('todo:todo_list')


class TodosView(DetailView):  # correct
    """Displays all tasks"""

    model = TodoList
    template_name = 'todo/todos.html'


class CreateTodoView(CreateView):  # correct
    """Create a task"""

    model = Todo
    template_name = 'todo/create_todo.html'
    fields = ['name_of_todo', 'task']
    todo_list = None

    def get_todo_list(self):
        list_pk = self.kwargs.get('list_pk', None)
        self.todo_list = get_object_or_404(TodoList, pk=list_pk)

    def get_context_data(self, **kwargs):
        self.get_todo_list()
        context = super(CreateTodoView, self).get_context_data(**kwargs)
        context['todo_list'] = self.todo_list
        return context

    def form_valid(self, form):
        self.get_todo_list()
        form.instance.todo_list = self.todo_list
        return super(CreateTodoView, self).form_valid(form)


class EditTodoView(UpdateView):  # correct
    """Edits the current task"""

    model = Todo
    template_name = 'todo/edit_todo.html'
    fields = ['task', 'status']

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(EditTodoView, self).form_valid(form)


class RemoveTodoView(DeleteView):  # correct
    """Removes the current task"""

    model = Todo
    template_name = 'todo/remove_todo.html'
    success_url = reverse_lazy('todo:todo_list')


class TodoDetailView(DetailView):  # correct
    """Detailed view of the current task"""

    model = Todo
    template_name = 'todo/todo_detail.html'

    # def form_valid(self, form):
    #     form.instance.owner = self.request.user
    #     return super(TodoDetailView, self).form_valid(form)
